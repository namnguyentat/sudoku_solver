require 'spec_helper'
require "sudoku_solver"
require_relative 'cases/sudoku_solver_cases'

describe 'SodukoSolver' do
  context 'when user pass invalid matrix' do
    SudokuSolverCases::INVALID_MATRIX.each do |invalid_case|
      it "should raise exception `invalid matrix` when pass #{invalid_case}" do
        expect { SudokuSolver.solve_sudoku(invalid_case) }.
          to raise_error(InvalidMatrix)
      end
    end
  end

  context 'when user pass valid but can not be resolved matrix' do
    SudokuSolverCases::IMPOSSIBLE_MATRIX.each do |impossible_case|
      it "should return `nil` when pass #{impossible_case}" do
        expect(SudokuSolver.solve_sudoku(impossible_case)).to be_nil
      end
    end
  end

  context 'when user pass valid and possible matrix' do
    SudokuSolverCases::VALID_POSSIBLE_MATRIX.each do |question, answer|
      it "should return `#{answer}` when pass `#{question}`" do
        expect( SudokuSolver.solve_sudoku(question) ).
          to eq(answer)
      end
    end
  end
end