#Sudoku Solver
###This is a program which solve Sudoku puzzle
* 9x9 Grid
* Sudoku solving algorithm: **[Backtracking](https://en.wikipedia.org/wiki/Sudoku_solving_algorithms)**
---
To use it, simply:
```
$ ruby sudoku_solver_command_line.rb '394__267_|___3__4__|5__69__2_|_45___9__|6_______7|__7___58_|_1__67__8|__9__8___|_264__735'
+-----+-----+-----+
|3 9 4|8 5 2|6 7 1|
|2 6 8|3 7 1|4 5 9|
|5 7 1|6 9 4|8 2 3|
+-----+-----+-----+
|1 4 5|7 8 3|9 6 2|
|6 8 2|9 4 5|3 1 7|
|9 3 7|1 2 6|5 8 4|
+-----+-----+-----+
|4 1 3|5 6 7|2 9 8|
|7 5 9|2 3 8|1 4 6|
|8 2 6|4 1 9|7 3 5|
+-----+-----+-----+
```